# Online pet-shop

- [Demo](https://react-hws-robinkite-936d310d7f5d8e856105101f2a6132d2543c8673519.gitlab.io/)

## Завдання

Написати юніт тести для інтернет-магазину. Додати в програму використання Context API.

Для цього необхідно доповнити проект, створений у попередньому домашньому завданні homework5.

#### Технічні вимоги:

- Напишіть кілька юніт-тестів для тестування всіх кнопок та модальних вікон програми.
- Напишіть юніт тести для редьюсерів
- Напишіть кілька снепшот-тестів для тестування найпростіших компонентів
- Для тестування можна використати бібліотеки Jest і Testing Library.
- Додайте на сторінці списку товарів перемикач виду відображення товарів – показувати їх у вигляді таблиці або карток. Цей перемикач повинен працювати за допомогою Context API.
